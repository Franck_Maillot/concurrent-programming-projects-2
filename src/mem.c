/*****************************************************
 * Copyright Grégory Mounié 2008-2013                *
 * This code is distributed under the GLPv3 licence. *
 * Ce code est distribué sous la licence GPLv3+.     *
 *****************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "mem.h"

/** squelette du TP allocateur memoire */

void *zone_memoire = 0;

//
typedef struct ListeBuddy
{
    struct ListeBuddy *Suiv;
} *Buddy;

// initialisation de la table des zones libres
// chaque case represente une puissance de 2: min 2^0 / max 2^20
Buddy tzl[21];

// fonction d'initialisation du tableau tzl
void
init_tzl(Buddy tzl)
{
  int i = 0;
  for (i = 0 ; i < 21 ; i++)
  {
    tzl(i) = NULL;
  }
}

// fonction retournant la puissance de 2 superieure ou égale à l'entree
int
get_puissance(unsigned long size)
{
  int k = 0;
  unsigned long puiss = 1;
  // si la taille est negatif -> cas d'erreur
  if (size < 0)
  {
    return -1;
  }
  // si la taille est 1: case 2^0
  if (size == 1)
  {
    return 0;
  }
  while (k < 20) {
    k++; // on passe à la puissance supérieure
    puiss = puiss * 2;
    if (size <= puiss)
    {
      return k;
    }
  }
  // cas où on ne trouve pas de puissance de deux suffisante
  return -1; 
}

int 
mem_init()
{
  if (! zone_memoire)
    zone_memoire = (void *) malloc(ALLOC_MEM_SIZE);
  if (zone_memoire == 0)
    {
      perror("mem_init:");
      return -1;
    }
  // initialisation de la table des zones libres: dans tout les cases on a 'null'
  init_tzl(tzl);
  // l'adresse de la  zone memoire alloue est dans la case qui represente 2^20
  tzl[20] = zone_memoire;
  return 0;
}

void *
mem_alloc(unsigned long size)
{
  // teste si la largeur est possible et trouve une zone de memoire adequate
  int taille = get_puissance(size);
  if (taille == -1)
  {
    return 0;
  }
  if (tzl[taille])
  {
    return tzl + taille;
  }
  // zone pas encore cree -> tzl[taille] est egal à NULL
  else
  {
    
}

int 
mem_free(void *ptr, unsigned long size)
{
  
  return 0;
}


int
mem_destroy()
{
  

  free(zone_memoire);
  zone_memoire = 0;
  return 0;
}

